<?php
define('WEBROOT' , str_replace('index.php' , '' , $_SERVER['SCRIPT_NAME']));
define('ROOT' , str_replace('index.php' , '' , $_SERVER['SCRIPT_FILENAME']));
require(ROOT.'core/Controller.php');

$param = explode('/',$_GET['p']);
$controller = $param[0] ;

if (isset($_COOKIE['admin']) && $_COOKIE['admin'] == "TRUE"){
    require(ROOT.'controllers/admin.php');
    $myctrl = new admin(); 
    if ($controller !== "admin" || !isset($controller)) {
        $myctrl->home();
    } elseif (!isset($param[1])){
        $myctrl->home();
    } else {
        $action = $param[1];
        if (method_exists( $controller , $action )) { 
            if ($action === "read" || $action === "delete" || $action === "update" || $action === "page"){
                $myctrl->$action($param[2]);
            } else {
                $myctrl->$action();
            }    
        } else {
            $myctrl = new $controller(); 
            $myctrl->home();
        }
    }
} else {
    if ($controller !== "article" || !isset($controller)) {
        require('controllers/article.php');
        $myctrl = new article(); 
        $myctrl->home();
    } elseif (!isset( $param[1])){
        require('controllers/'.$controller.'.php');
        $myctrl = new $controller(); 
        $myctrl->home();
    } else {
        require('controllers/'.$controller.'.php');
        $myctrl = new $controller();
        $action = $param[1];
        if (method_exists( $controller , $action )) { 
            if ($action === "read" || $action === "page"){
                $myctrl->$action($param[2]);
            } else {
                $myctrl->$action();
            }    
        } else {
            $myctrl = new $controller(); 
            $myctrl->home();
        }
    }
}

