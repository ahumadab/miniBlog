<?php
require('/var/www/vp.com/models/ArticleModel.php');

class admin extends Controller 
{   

    public function navigation($id=0)
    {
        $d["link"] = array(
            "post" => "admin",
            "home" => '<a href="/admin/home">Home</a> ',
            "list" => '<a href="/admin/list">List</a> ',
            "create" => '<a href="/admin/create">Create Article</a> ',
            "delete" => '<form action="/admin/delete/'.$id.'" method="post"><button type="submit">Delete</button></form>',
            "modify" => '<form action="/admin/update/'.$id.'" method="post"><button type="submit">Modify</button></form>'
        );
        return $d["link"];
    }

    public function create()
    {
        if ($_POST)
        {
            // var_dump($_POST);
            $title = $_POST['title'];
            $content = $_POST['content'];
            $date = $_POST['date'];
            $category = $_POST['category'];
            $author = $_COOKIE['id'];
            $article = new ArticleModel;
            $article->setNewArticle($title, $content, $date, $category, $author);

            $d['create']= array(
                "message" => "Article créé"
            );
            $this->set($d);
        }
        $this->render('create');
    }

    public function list()
    {   
        $article = new ArticleModel;
        if (isset($_POST["submit"])){
            $author = $_POST["author"];
            $date = $_POST["date"];

            if(isset($_POST["category"])){
                $category = $_POST["category"];
            } else {
                $category = FALSE;
            }

            switch (true){
                case (!empty($author) && !empty($date) && $category) :
                    $result = $article->getAllArticleTitle($author, $date, $category);
                    break;
                case (!empty($author) && !empty($date)) :
                    $result = $article->getAllArticleTitle($author,$date,null);
                    break;
                case (!empty($author) && $category) :
                    $result = $article->getAllArticleTitle($author,null,$category);
                    break;
                case (!empty($date) && $category) :
                    $result = $article->getAllArticleTitle(null,$date,$category);
                    break;
                case (!empty($author)) :
                    $result = $article->getAllArticleTitle($author,null,null);
                    break;
                case (!empty($date)) :
                    $result = $article->getAllArticleTitle(null,$date,null);
                    break;
                case ($category) :
                    $result = $article->getAllArticleTitle(null,null,$category);
                    break;
                default:
                $result = $article->getAllArticleTitle($author, $date, $category);
            }
        } else {
            $result = $article->getAllArticleTitle();
        }


        for ($i=0; $i<count($result); $i++){
            $d['list'][$i]= array($result[$i]['title'], $result[$i]['id']);
        }
        if ($result == FALSE){
            $d['message'] = array(
                "error" => "Aucun article enregisté",
            );
        }
        $d['link'] = $this->navigation();
        $this->set($d);
        $this->render('list');
    }

    public function read($p)
    {
        $article = new ArticleModel;
        $result = $article->getArticleByNumber($p);
        $d['read'] = array(
            "title" => $result["title"],
            "content" => $result["content"],
            "date" => $result["date"],
            "author" => $result["username"],
            "category" => $result["name"]
        );  

        $d["link"] = array(
            "id" => $p,
            "delete" => '<form action="/admin/delete/'.$p.'" method="post"><button type="submit">Delete</button></form>',
            "modify" => '<form action="/admin/update/'.$p.'" method="post"><button type="submit">Modify</button></form>'
        );
        $this->set($d);
        $this->render('read');
    }

    public function update($id) 
    {
        $article = new ArticleModel;
        if ($_POST){
            $article->updateArticle($id, $_POST);
            $this->read($id);
        } else {
            $result = $article->getArticleByNumber($id);
            $d['update'] = array(
                "id" => $result['id'],
                "title" => $result["title"],
                "content" => $result["content"],
                "date" => $result["date"],
                "category" => $result["name"]
            );
            $this->set($d);
            $this->render('create');
        }
        
    }

    public function delete($id) 
    {
        $article = new ArticleModel;
        $article->deleteArticle($id);

        $d['delete'] = array(
            "message" => "Article supprimé"
        );

        $this->set($d);
        $this->list();
    }

    public function home()
    {
        $d["home"] = array(
            "title" => "Bienvenue sur MonBlog.com",
            "content" => "Ici c'est VOTRE blog ! Vous pouvez créez votre blog à volonté",
            "date" => "2012"
        );
        $d["link"]=$this->navigation();
        $this->set($d);
        $this->render('home');

        $article = new ArticleModel;
        $result = $article->getLastArticle(5);

        foreach ($result as $key=>$value){
            $this->read($value["id"]);
        }
    }

    public function disconnect()
    {
        //unset($_COOKIE['admin']);
        setcookie("admin", "TRUE", 1, "/");
        header('Location: http://vp.com/article/home');
    }

}

