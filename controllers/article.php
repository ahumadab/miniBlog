<?php
require('/var/www/vp.com/models/ArticleModel.php');

class Article extends Controller 
{   

    public function list()
    {   
        $article = new ArticleModel;
        if (isset($_POST["submit"])){
            $author = $_POST["author"];
            $date = $_POST["date"];

            if(isset($_POST["category"])){
                $category = $_POST["category"];
            } else {
                $category = FALSE;
            }

            switch (true){
                case (!empty($author) && !empty($date) && $category) :
                    $result = $article->getAllArticleTitle($author, $date, $category);
                    break;
                case (!empty($author) && !empty($date)) :
                    $result = $article->getAllArticleTitle($author,$date,null);
                    break;
                case (!empty($author) && $category) :
                    $result = $article->getAllArticleTitle($author,null,$category);
                    break;
                case (!empty($date) && $category) :
                    $result = $article->getAllArticleTitle(null,$date,$category);
                    break;
                case (!empty($author)) :
                    $result = $article->getAllArticleTitle($author,null,null);
                    break;
                case (!empty($date)) :
                    $result = $article->getAllArticleTitle(null,$date,null);
                    break;
                case ($category) :
                    $result = $article->getAllArticleTitle(null,null,$category);
                    break;
                default:
                $result = $article->getAllArticleTitle($author, $date, $category);
            }
        } else {
            $result = $article->getAllArticleTitle();
        }

        for ($i=0; $i<count($result); $i++){
            $d['list'][$i]= array($result[$i]['title'], $result[$i]['id']);
        }
        if ($result == FALSE){
            $d['message'] = array(
                "error" => "Aucun article enregisté"
            );
        }
        $d['link'] = $this->navigation();
        $this->set($d);
        $this->render('list');
    }

    public function navigation()
    {
        $d["link"] = array(
            "post" => "article",
            "home" => '<a href="/article/home">Home</a> ',
            "list" => '<a href="/article/list">List</a> '
        );
        return $d["link"];
    }

    public function read($p)
    {
        $article = new ArticleModel;
        $result = $article->getArticleByNumber($p);
        $d['read'] = array(
            "title" => $result["title"],
            "content" => $result["content"],
            "date" => $result["date"],
            "author" => $result["username"],
            "category" => $result["name"]
        );  
        $this->set($d);
        $this->render('read');
    }

    public function home()
    {
        $d["home"] = array(
            "title" => "Bienvenue sur MonBlog.com",
            "content" => "Ici c'est VOTRE blog ! Vous pouvez créez votre blog à volonté (si vous êtes admin)",
            "date" => "2012"
        );
        $d["link"] = $this->navigation();
        $this->set($d);
        $this->render('home');

        $article = new ArticleModel;
        $result = $article->getLastArticle(5);
        foreach ($result as $key=>$value){
            $this->read($value["id"]);
        }
    }

    public function login()
    {
        $username = $_POST["username"];
        $pwd = $_POST["password"];

        $sql="SELECT * FROM admin WHERE username='$username' AND password='$pwd'";
        $result = $this->get($sql, "fetch");

        if ($result){
            echo 'ok';
            setcookie("admin", "TRUE", strtotime("1 year"), '/');
            setcookie("id", $result['id'], strtotime("1 year"), '/');
            header('Location: http://vp.com/admin/home');
        } else {
            header('Location: http://vp.com/article/home');
        }
    }


}
