<?php

// namespace App\Controller;

class Controller {
    public $_vars = array();
    public $_head = array();
    public $_mainContent = array();
    public $_footer = array();

    public function set($d) 
    {
        $this->_vars = array_merge( $this->_vars , $d );
    }

    public function get($sql, $fetch)
    {
        require("/var/www/vp.com/keyBDD.php");
        $pdo = new PDO($dsn, $user, $password);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
        $sth = $pdo->prepare($sql);
        $sth->execute();
        $result = $sth->$fetch(PDO::FETCH_ASSOC);
        return $result;
    }
    

    public function render($filename)
    {
        extract($this->_vars);
        require(ROOT.'views/'.$filename.'.php');
    }

    public function page($p)
    {
        $d["link"] = $this->navigation();
        $this->set($d);
        $this->read($p);
    } 

}


