<html>
    <head>
        <title><?=$home['title']?></title>
        <link rel="stylesheet" type="text/css" href="/CSS/index.css" media="all"/>
    </head>
    <body>
        <?php 
            if (isset($_COOKIE['admin'])) {
                require(ROOT.'views/disconnect.php');
            } else {
                require(ROOT.'views/login.php');
            }
        ?>
        <div class="MainContent">
            <h1><?=$home['title']?></h1>
            <p><?=$home['content']?></p>
            <p><?=$home['date']?></p>
        </div>
        <div>
            <h3>Navigation:</h3>
        <?php 
            if (isset($link["create"])){
                echo $link["create"].$link["list"];
            } elseif (isset($link["list"])) {
                echo $link["list"];
            }
        ?>
        </div>
        
        

        <script>
            // Get the modal
            var modal = document.getElementById('login');
            // When the user clicks anywhere outside of the modal, close it
            window.onclick = function(event) {
                if (event.target == modal) {
                    modal.style.display = "none";
                }
            }
        </script>
    </body>
</html>