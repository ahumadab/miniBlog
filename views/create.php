<?php
if (isset($update)){
    ?>
    <form action="/admin/update/<?=$update['id']?>" method="post">
        <textarea id="mytext" name="content" rows="20" cols="50" required><?=$update['content']?></textarea>
        <br>
        <input type="text" name="title" placeholder="Titre" value="<?=$update['title']?>" required>
        <input type="text" name="date" pattern="[0-9]{4}" placeholder="Date YYYY" value="<?=$update['date']?>" required>
        <select name="category" id="">
            <option value="1">Sport</option>
            <option value="2">Storytime</option>
            <option value="3">Anime/Manga</option>
            <option value="4">Food</option>
            <option value="5">Videogame</option>
        </select>
        <input type="submit">
    </form>
    <?php
} else {
    ?>
    <form action="/admin/create" method="post">
        <textarea id="mytext" name="content" rows="20" cols="50" required></textarea>
        <br>
        <input type="text" name="title" placeholder="Titre" required>
        <input type="text" name="date" pattern="[0-9]{4}" placeholder="Date YYYY" required>
        <select name="category" id="">
            <option value="1">Sport</option>
            <option value="2">Storytime</option>
            <option value="3">Anime/Manga</option>
            <option value="4">Food</option>
            <option value="5">VideoGame</option>
        </select>

        <input type="submit">
    </form>
    <?php
}
?>



<div class="message">
    <p>
        <?php 
            if (isset($create)){
                echo $create["message"];
            }
        ?>
    </p>
</div>


<a href="/article/list">List</a>
<a href="/article/home">Home</a>