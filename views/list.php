<ul><h1>Liste d'articles</h1>
<form action="/<?=$link["post"]?>/list/" method="post">
    <h3>Filtrer par :</h3>
    <input type="text" name="author" placeholder="Auteur" autocomplete="">
    <input type="text" name="date" pattern="[0-9]{4}" placeholder="Date YYYY">
    <select name="category">
        <option disabled selected>Catégorie</option>
        <option value="Sport">Sport</option>
        <option value="Storytime">Storytime</option>
        <option value="Anime/Manga">Anime/Manga</option>
        <option value="Food">Food</option>
        <option value="VideoGame">Videogame</option>
    </select>
    <button type="submit" name="submit">Valider</button>
</form>
    <?php
        if (isset($message["error"])){
            echo $message["error"];
        } else {
            foreach($list as $key=>$value){
                echo "<li><a href='/".$link["post"]."/page/".$value[1]."'>".$value[0]."</a></li>";
            }
        }
        
    ?>
</ul>
<br>
<div class="message">
    <p>
        <?php 
            if (isset($delete)){
                echo $delete["message"];
            }
        ?>
    </p>
</div>
<?php 
    if (isset($link["create"])){
        echo $link["create"];
        echo $link["home"];
    } else {
        echo $link["home"];
    }
?>

