<button onclick="document.getElementById('login').style.display='block'" style="width:auto;">Admin</button>

<!-- Formulaire de connexion -->
<div id="login" class="modal">
  <form class="modal-content animate" action="/article/login" method="POST">
    <div class="imgcontainer">
      <span onclick="document.getElementById('login').style.display='none'" class="close" title="Close Modal">&times;</span>
      <!-- <img src="logo.png" alt="Avatar" class="avatar"> -->
    </div>

    <div class="container">
      <label for="username"><b>Username</b></label>
      <input type="text" placeholder="Username" name="username">

      <label for="password"><b>Password</b></label>
      <input type="password" placeholder="Password" name="password">
        
      <button type="submit" name="submit">Login</button>
      <label>
        <input type="checkbox" checked="checked" name="remember"> Se souvenir de moi
      </label>
    </div>
  </form>
</div>