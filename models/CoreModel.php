<?php

class CoreModel
{
    public function get($sql, $fetch)
    {
        require("/var/www/vp.com/keyBDD.php");
        $pdo = new PDO($dsn, $user, $password);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
        $sth = $pdo->prepare($sql);
        $sth->execute();
        $result = $sth->$fetch(PDO::FETCH_ASSOC);
        return $result;
    }
    public function set($sql)
    {
        require("/var/www/vp.com/keyBDD.php");
        $pdo = new PDO($dsn, $user, $password);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
        $sth = $pdo->prepare($sql);
        $sth->execute();
    }
}