<?php
require('/var/www/vp.com/models/CoreModel.php');

class ArticleModel extends CoreModel
{
    public $id;
    public $title;
    public $date;
    public $author;
    public $category;
    public $content;
    public $result;

    public function getArticleByNumber($p)
    {
        $sql="SELECT * 
        FROM article 
            INNER JOIN admin 
                ON article.author = admin.id 
            INNER JOIN category
                ON article.id_category = category.id
        WHERE article.id=$p";
        $result = $this->get($sql, "fetch");
        return $result;
    }

    public function getAllArticleTitle($author = null, $date = null, $category = null)
    {

        switch (true){
            case ($author!==null && $date!==null && $category!==null) :
                $sql="SELECT title, id FROM article WHERE author='$author' AND date='$date' AND category='$category'";
                break;
            case ($author!==null && $date!==null) :
                $sql="SELECT title, id FROM article WHERE author='$author' AND date='$date'";
                break;
            case ($author!==null && $category!==null) :
                $sql="SELECT title, id FROM article WHERE author='$author' AND category='$category'";
                break;
            case ($date!==null && $category=null) :
                $sql="SELECT title, id FROM article WHERE date='$date' AND category='$category'";
                break;
            case ($author!==null) :
                $sql="SELECT title, id FROM article WHERE author='$author'";
                break;
            case ($date!==null) :
                $sql="SELECT title, id FROM article WHERE date='$date'";
                break;
            case ($category!==null) :
                $sql="SELECT title, id FROM article WHERE category='$category'";
                break;
            default:
                $sql="SELECT title, id FROM article";
        }
            $result = $this->get($sql, "fetchAll");
            return $result;
    }

    public function getLastArticle($value=null)
    {
        if ($value!==null) {
            $sql="SELECT id FROM article ORDER BY date DESC LIMIT $value";
        } else {
            $sql="SELECT id FROM article ORDER BY date DESC";
        }
        $result = $this->get($sql, "fetchAll");
        return $result;
    }

    public function setNewArticle($title, $content, $date, $category, $author)
    {
        $sql= "INSERT INTO article (content, title, date, id_category, author) 
        VALUES ('$content', '$title', '$date', '$category', '$author');";
        $this->set($sql);
    }

    public function updateArticle($id, $values)
    {
        ['content' => $content, 
        'title' => $title, 
        'author' => $author, 
        'date' => $date, 
        'category' => $category] = $values;

        $sql="UPDATE article SET 
                title='$title',
                content='$content',
                date='$date',
                category='$category',
                author='$author'
                WHERE id='$id'";
        $this->set($sql);
    }

    public function deleteArticle($id)
    {
        $sql="DELETE FROM article WHERE id=$id";
        $this->set($sql);
    }
}